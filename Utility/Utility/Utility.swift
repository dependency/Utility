//
//  Utility.swift
//  Utility
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import Foundation

public func describe(int: Int) -> String {
    return "The integer \(int)"
}