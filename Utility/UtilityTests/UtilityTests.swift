//
//  UtilityTests.swift
//  UtilityTests
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import XCTest
import Tester
@testable import Utility

class UtilityTests: XCTestCase {
    
    func testQuadruple() {
        assert(quadruple(1) == 4)
    }
    
}
